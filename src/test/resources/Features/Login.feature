Feature: Demo web shop validation

Scenario: To validate the demo web shop login functionality
  Given open the website
  And The homepage of the website get displayed
	When Click on the login profile button
	Then Click  the login button
	Then The user should taken to the homepage of the demo web shop
	
  @books
  Scenario: Books
    Given open demo web shop home page
    When  select the Books category
    And I sort the books by Price 'High to Low'
    And I add two books to the cart
    Then the two books should be added to the cart successfully

  @electronics
  Scenario: Electronics
    Given open demo web shop home page
    When select the Electronics category
    And  select a cell phone product
    And add the product to the cart
    Then the count of items added to the cart should be displayed successfully

  @giftcards
  Scenario: Gift Cards
    Given open demo web shop home page
    When I select the Gift Cards category
    And I display 4 items per page
    And I capture the name and price of one of the displayed gift cards
    Then the name and price should be captured successfully

  @logout
  Scenario: Logout
    Given I am logged in to the demo web shop
    When I click the logout button
    Then I should be logged out successfully
    And the login button should be displayed on the home page
	 