package Stepdefinition;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

public class TestReports {

	@CucumberOptions(tags = "", features ="src/test/resources/Features/Login.feature",
			glue = "stepTest",
			plugin = { "pretty", "html:target/cucumber-reportss" },
			monochrome = true)
			public class TestRunnerClass extends AbstractTestNGCucumberTests{
			    }
}
