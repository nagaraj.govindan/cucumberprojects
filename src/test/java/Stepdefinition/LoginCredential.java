package Stepdefinition;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginCredential {
	WebDriver driver;
	@Given("open the website")
	public void open_the_website() {
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demowebshop.tricentis.com/login");

		System.out.println("lauching the browser");
	}

	@Given("The homepage of the website get displayed")
	public void the_homepage_of_the_website_get_displayed() {
		System.out.println("Home is displayed");

	}
	@When("Click on the login profile button")
	public void click_on_the_login_profile_button() {
		System.out.println("click login profile button");
	}
	@When("enter the username and password")
	public void enter_the_username_and_password() {
		WebElement email = driver.findElement(By.id("Email"));
		email.sendKeys("n95000030320@gmail.com");
		WebElement password = driver.findElement(By.id("Password"));
		password.sendKeys("Naga@5565");
	}


	@Then("Click  the login button")
	public void click_the_login_button() {
		WebElement loginButton = driver.findElement(By.xpath("//a[@class='ico-login']"));
		loginButton.click();

	}

	@Then("The user should taken to the homepage of the demo web shop")
	public void the_user_should_taken_to_the_homepage_of_the_demo_web_shop() {
		System.out.println("homepage of demo web shop sholud be displayed");
	}

	@Given("open demo web shop home page")
	public void open_demo_web_shop_home_page() {
		driver = new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com/");
	}

	@When("select the Books category")
	public void select_the_books_category() {
		WebElement booksLink = driver.findElement(By.cssSelector("a[href='/books']"));
		booksLink.click();
	}

	@When("I sort the books by Price {string}")
	public void i_sort_the_books_by_price(String string) {
		WebElement sortBy = driver.findElement(By.id("products-orderby"));
		sortBy.sendKeys("Price: High to Low");
	}

	@When("I add two books to the cart")
	public void i_add_two_books_to_the_cart() {

		WebElement firstBook = driver.findElement(By.xpath("(//input[@class='button-2 product-box-add-to-cart-button'])[1]"));
		firstBook.click();
		WebElement secondBook = driver.findElement(By.xpath("(//input[@class='button-2 product-box-add-to-cart-button'])[2]"));
		secondBook.click();

	}

	@Then("the two books should be added to the cart successfully")
	public void the_two_books_should_be_added_to_the_cart_successfully() {
		WebElement cartIcon = driver.findElement(By.xpath("//span[@class='cart-qty']"));
		cartIcon.getText();
		driver.quit();


	}
	@When("select the Electronics category")
	public void select_the_electronics_category() throws InterruptedException {
		WebElement electronicsLink = driver.findElement(By.xpath("//a[@href='/electronics']"));
		electronicsLink.click();
		Thread.sleep(4000);
	}

	@When("select a cell phone product")
	public void select_a_cell_phone_product() throws InterruptedException {
	
		 WebElement cellPhonesLink = driver.findElement(By.xpath("(//a[@title='Show products in category Cell phones'])[1]"));
	        cellPhonesLink.click();
	
		Thread.sleep(2000);
	}

	@When("add the product to the cart")
	public void add_the_product_to_the_cart() {
		 WebElement product = driver.findElement(By.xpath("(//input[@class='button-2 product-box-add-to-cart-button'])[1]"));
	        product.click();
	}

	@Then("the count of items added to the cart should be displayed successfully")
	public void the_count_of_items_added_to_the_cart_should_be_displayed_successfully() {
		WebElement cart=driver.findElement(By.className("cart-qty"));
		String itemcount=(cart.getText());
		System.out.println(itemcount);
		driver.quit();

	}

	@When("I select the Gift Cards category")
	public void i_select_the_gift_cards_category() throws InterruptedException {

		WebElement giftcard = driver.findElement(By.xpath("//a[@href='/gift-cards']"));
		giftcard.click();
	}

	@When("I display {int} items per page")
	public void i_display_items_per_page(Integer int1) {
		WebElement itemsPerPageDropdown = driver.findElement(By.id("products-pagesize"));
		itemsPerPageDropdown.sendKeys(String.valueOf(int1));
	}




	@When("I capture the name and price of one of the displayed gift cards")
	public void i_capture_the_name_and_price_of_one_of_the_displayed_gift_cards() {
		// Select the first gift card product
		WebElement firstProduct = driver.findElement(By.xpath("//a[@href='/5-virtual-gift-card']"));
		firstProduct.click();

		// Capture the name and price
		WebElement productName = driver.findElement(By.xpath("//h1[@itemprop='name']"));
		String name = productName.getText();

		WebElement productPrice = driver.findElement(By.xpath("//span[@itemprop='price']"));
		String price = productPrice.getText();
	}

	@Then("the name and price should be captured successfully")
	public void the_name_and_price_should_be_captured_successfully() {
		WebElement productName = driver.findElement(By.xpath("//h1[@itemprop='name']"));
		String name = productName.getText();

		WebElement productPrice = driver.findElement(By.xpath("//span[@itemprop='price']"));
		String price = productPrice.getText();
		driver.quit();
	}

	@Given("I am logged in to the demo web shop")
	public void i_am_logged_in_to_the_demo_web_shop() {
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demowebshop.tricentis.com/login");
		// driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);  
	}

	@When("I click the logout button")
	public void i_click_the_logout_button() {
		driver.findElement(By.id("Email")).sendKeys("n95000030320@gmail.com");
		driver.findElement(By.id("Password")).sendKeys("Naga@5565");
		driver.findElement(By.xpath("//input[@value='Log in']")).click();

	}

	@Then("I should be logged out successfully")
	public void i_should_be_logged_out_successfully() {
		driver.findElement(By.xpath("//a[@href='/logout']")).click();

	}

	@Then("the login button should be displayed on the home page")
	public void the_login_button_should_be_displayed_on_the_home_page() {
		WebElement userName = driver.findElement(By.linkText("Log in"));
		Assert.assertTrue(userName.isDisplayed());
		driver.quit();
	}

}




